/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package tecnochat;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

// Se agregan las librerias para conectar con la BD
import java.sql.Connection;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.Vector;
/**
 *
 * @author gmendez
 */
public class Servidor {
    ArrayList<Conexion> conexiones;
    ServerSocket   ss;
    Connection conn;
    

    Vector<String> usuarios = new Vector<>();
    
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                (new Servidor()).start();
            }
        });
    }

    private void start() {
        this.conexiones = new ArrayList<>();
        Socket socket;
        Conexion cnx;
        
        try {
            // Se realiza una sola conexion
            if (conn == null){
                conn = DriverManager.getConnection("jdbc:mysql://localhost/tecnochat","root","");
            }
            
            ss = new ServerSocket(4444);
            System.out.println("Servidor iniciado, en espera de conexiones");
            
            while (true){             
                socket = ss.accept();
                cnx = new Conexion(this, socket, conexiones.size(),this.conn);
                conexiones.add(cnx);
                cnx.start();                
            }            
            
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }  catch (SQLException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    // Broadcasting
    private void difundir(String id, String mensaje) {
        Conexion hilo;
        for (int i = 0; i < this.conexiones.size(); i++){
            hilo = this.conexiones.get(i);
            if (hilo.cnx.isConnected()){
                if (!id.equals(hilo.id)){
                    hilo.enviar(mensaje);
                }
            }
        }//To change body of generated methods, choose Tools | Templates.
    }
    
    class Conexion extends Thread {
        BufferedReader in;
        PrintWriter    out;
        Connection     conn;
        Socket         cnx;
        Servidor       padre;
        int            numCnx = -1;
        String         id = "";
        
        public final int SIN_USER   = 0;
        public final int USER_IDENT = 1;
        public final int PASS_PDTE  = 2;
        public final int PASS_OK    = 3;
        public final int CHAT       = 4;
                
        public Conexion(Servidor padre, Socket socket, int num, Connection _conn){
            this.conn = _conn;
            this.cnx = socket;
            this.padre = padre;
            this.numCnx = num;
            this.id = socket.getInetAddress().getHostAddress()+"-"+num;
        }
        
        @Override
        public void run() {
            String linea="", user="", pass="", mensaje="";
            String passValido = "";
            int estado = SIN_USER;
            int usr = -1;
            int intentos = 3;
            
            Statement stmt;
            ResultSet rset;

            String sQuery = "";
            
            try {
                in = new BufferedReader(new InputStreamReader(cnx.getInputStream()));
                out = new PrintWriter(cnx.getOutputStream(),true);
                
                System.out.printf("Aceptando conexion desde %s\n",
                        cnx.getInetAddress().getHostAddress());
                                
                while(!mensaje.toLowerCase().equals("salir") && intentos>0){                    
                    switch (estado){
                        case SIN_USER:
                            out.println("Bienvenido, proporcione su usuario");
                            estado = USER_IDENT;
                            break;
                        case USER_IDENT:
                            user = in.readLine();
                            boolean found = false;
                            passValido = "";
                            sQuery = "SELECT usuario,contrasena FROM usuarios WHERE usuario = '"+user.trim()+"'";
                            
                            try {
                                stmt = conn.createStatement();
                                rset = stmt.executeQuery(sQuery);
                                
                                if (rset.next()){
                                    found = true;
                                    passValido = rset.getString("contrasena");
                                }
                                
                            } catch (SQLException sqle){
                                
                            }
                            
                            if (!found){
                                estado = SIN_USER;
                            } else {
                                estado = PASS_PDTE;
                            }                                                                                                             
                            break;
                        case PASS_PDTE:
                            out.println("Escriba la contrasena");
                            pass = in.readLine();                            
                            if (pass.equals(passValido)){
                                estado = PASS_OK;
                                usuarios.addElement(user);
                            }
                            --intentos;
                            break;
                        case PASS_OK:
                            out.println(" Autentificado ");
                            out.println(convertirUsuario());
                            difundir(id,user+" Se ha conectado");
                            estado = CHAT;
                            break;
                        case CHAT:
                            mensaje = in.readLine();
                            System.out.printf("%s - %s\n",
                                        cnx.getInetAddress().getHostAddress(),
                                        user + ":" +mensaje);
                            
                            this.padre.difundir(this.id, this.id+" | "+user+" : "+mensaje);
                            break;
                    }                        
                } 
                difundir(id, user+ " Se desconecto");
                removerUsuario(user);
                this.cnx.close();
                System.out.println("Cerrando conexion: "+this.id);
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }            
        }
        
        public String convertirUsuario(){
            String personas = "";
            
            for (int i = 0; i < usuarios.size(); i++) {
                String p = usuarios.get(i);
                personas = personas + p + "|";
            }
            return personas;
        }

        private void enviar(String mensaje) {
            this.out.println(mensaje); //To change body of generated methods, choose Tools | Templates.
        } 
        
        public void removerUsuario(String eliminado){
            usuarios.removeElement(eliminado);
        }
    }   
}



